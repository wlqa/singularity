Singularity可以方便地处理Docker镜像，以下是三种方式：
#### 方式1：从docker uri开始
参考：https://sylabs.io/guides/3.7/user-guide/singularity_and_docker.html#remotely-hosted-images

有以下两个命令可用：


```
singularity pull docker://godlovedc/lolcow # 下载pre-built image
singularity build mylolcow_latest.sif docker://godlovedc/lolcow # 下载后再build成镜像
```


#### 方式2：从本地缓存的docker image开始
参考：https://sylabs.io/guides/3.7/user-guide/singularity_and_docker.html#locally-available-images-cached-by-docker

假设本地有godlovedc/lolcow镜像

```

$docker images
REPOSITORY      TAG       IMAGE ID       CREATED        SIZE
ansys-centos7   18        2aa93b7916c5   13 hours ago   28.7GB

```

那么通过如下命令可从本地docker镜像构建SIF文件：

```
sudo singularity build ansys18.sif docker-daemon://ansys-centos7:18
INFO:    Starting build...
Getting image source signatures
Copying blob d9b2938c1cab done  
Copying config ae8ce7defa done  
Writing manifest to image destination
Storing signatures
2022/04/14 20:10:58  info unpack layer: sha256:220c36e95d2b2cdfb18ab2bdf0fd2e73c302957933526ada0a65b4a3207ede6d

```



该命令与方式1的主要区别有：

> 从原来的docker变成了docker-daemon；

> 这里需要加sudo，因为docker程序运行时需要sudo权限；

> docker的镜像后面必须要加TAG标签，可以通过sudo docker images查看TAG标签。


#### 方式3：从镜像tar文件开始
参考：https://sylabs.io/guides/3.7/user-guide/singularity_and_docker.html#locally-available-images-stored-archives

首先需要将docker镜像转成tar文件：

先查看当前的docker镜像


```
$ sudo docker images
REPOSITORY           TAG              IMAGE ID       CREATED        SIZE  
hello-world          latest           d1165f221234   5 months ago   13.3kB
```

使用如下命令将hello-world镜像转成tar文件，这里save后面的参数就是IMAGE ID，即上面输出的第三列

`$ sudo docker save d1165f221234 -o hello_world.tar`

这样当前目录下面就出现了hello_world.tar文件。

接着，将该tar文件转成singularity镜像，命令与输出如下：


```
$ singularity build hello_world.sif docker-archive://hello_world.tar
INFO:    Starting build...
Getting image source signatures
Copying blob f22b99068db9 done
Copying config 1189c90721 done
Writing manifest to image destination
Storing signatures
2021/08/15 16:46:15  info unpack layer: sha256:a99912efe9c767b280c869d2e734bef3c92d29d45d4e3beefbeb5a1924ed7445
INFO:    Creating SIF file...
INFO:    Build complete: hello_world.sif
```


该命令与方式1的主要区别有：

> 原本的docker://换成了docker-archive://，方式2里面是docker-daemon://；

> 这里不需要加sudo了，只需要当前用户有hello_world.tar文件的可访问权限即可；

> 这里用的是tar文件，singularity也可以处理tar.gz文件。





