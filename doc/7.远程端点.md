#### 概述
`remote`命令组允许用户管理 Singularity 将与许多常见命令流交互的服务端点。此命令组管理三种主要类型的远程端点：公共 Sylabs Cloud（或本地 Singularity Enterprise 安装）、OCI 注册表和密钥服务器。

#### 公共教学云

Sylabs 推出了在线Sylabs Cloud，使用户能够创建、保护并与他人共享他们的容器镜像。

Singularity 的全新默认安装配置为连接到公共cloud.sylabs.io 服务。如果您只想使用公共服务，您只需要获取身份验证令牌，然后：singularity remote login

1. 前往：https ://cloud.sylabs.io/

2. 单击“登录”并按照登录步骤进行操作。

3. 单击您的登录 ID（与登录按钮相同且已更新）。

4. 从下拉菜单中选择“访问令牌”。

5. 输入新访问令牌的名称，例如“测试令牌”

6. 单击“创建新的访问令牌”按钮。

7. 在“新建 API 令牌”页面中单击“将令牌复制到剪贴板”。

8. 在提示符处运行并粘贴访问令牌。singularity remote login

存储令牌后，您可以使用以下子命令检查是否能够连接到服务status：

```
 singularity remote status
INFO:    Checking status of default remote.
SERVICE    STATUS  VERSION             URI
Builder    OK      v1.1.14-0-gc7a68c1  https://build.sylabs.io
Consent    OK      v1.0.2-0-g2a24b4a   https://auth.sylabs.io/consent
Keyserver  OK      v1.13.0-0-g13c778b  https://keys.sylabs.io
Library    OK      v1.0.16-0-gb7eeae4  https://library.sylabs.io
Token      OK      v1.0.2-0-g2a24b4a   https://auth.sylabs.io/token
INFO:    Access Token Verified!

Valid authentication token set (logged in).
```

您可以使用各种 Singularity 命令与公共 Sylabs Cloud 进行交互：

pull, push, build –remote, key, search, verify, exec, shell, run, instance


#### 管理远程端点

使用 docker://, oras:// and shub:// 带有这些命令的uri不会与Sylabs Cloud交互。

用户可以在多个远程端点之间设置和切换，这些端点存储在 ~/.singularity/remote.yaml . 或者，远程端点可以由管理员在系统范围内设置

通常，用户和管理员应该使用 singularity remote 命令, 和避免编辑 remote.yaml 直接配置文件。

























