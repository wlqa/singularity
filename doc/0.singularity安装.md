#### 在CentOS7.4环境下安装Singularirty3.9


#### 1、安装Singularity依赖的软件包


```
# sudo yum update -y && \
     sudo yum groupinstall -y 'Development Tools' && \
     sudo yum install -y \
     openssl-devel \
     libuuid-devel \
     libseccomp-devel \
     wget \
     squashfs-tools \
     cryptsetup

```

#### 2、安装go软件包
如下安装可能遇到软件不能下载的问题，Go下载 - Go语言中文网 - Golang中文社区了备选资源。


```
export VERSION=1.17.2 OS=linux ARCH=amd64 && \
wget https://dl.google.com/go/go$VERSION.$OS-$ARCH.tar.gz && \
sudo tar -C /usr/local -xzvf go$VERSION.$OS-$ARCH.tar.gz && \
rm go$VERSION.$OS-$ARCH.tar.gz

#设置go的环境变量
echo 'export GOPATH=${HOME}/go' >> ~/.bashrc && \
echo 'export PATH=/usr/local/go/bin:${PATH}:${GOPATH}/bin' >> ~/.bashrc && \
source ~/.bashrc

```
#### 3、下载Singularity

```
export VERSION=3.9.0-rc.3 && # adjust this as necessary \
wget https://github.com/sylabs/singularity/releases/download/v${VERSION}/singularity-ce-${VERSION}.tar.gz && \
tar -xzf singularity-ce-${VERSION}.tar.gz && \
cd singularity-ce-${VERSION}
```

#### 4、编译singularity并安装
默认安装到/usr/local目录，如需修改，请在mconfig --prefix=YOURPATH设置。

```
./mconfig && \
    make -C ./builddir && \
    sudo make -C ./builddir install

```
#### 5、测试安装完成的Singularity
`singularity run  library://lolcow`

#### 6、启用fakeroot功能
启用该功能之后，被授权的用户可以更自由的修改镜像文件。


```
echo user.max_user_namespaces=15000 >/etc/sysctl.d/90-max_net_namespaces.conf
sysctl -p /etc/sysctl.d /etc/sysctl.d/90-max_net_namespaces.conf
```

为Singularity添加用户

`#singularity config fakeroot --add zhangping `
查看添加是否成功

```
cat /etc/subuid  
1000:4294836224:65536 
#cat /etc/subgid 
1000:4294836224:65536
```













